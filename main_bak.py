__author__ = 'bmj'

import os
import webapp2
import jinja2
import re
import json
import hmac
import hashlib
from markdown_dir import markdown
#from pygments_dir import pygments
from pybcrypt import bcrypt
from google.appengine.ext import ndb
from secret import secret

"""
Open Issues:
 -search engine friendly url's, instead of post_id

Closed:
 -must have an edit feature to edit the posts.

"""

################################################
####### U4      (Cookie HASHING)       #########
################################################

# http://en.wikipedia.org/wiki/HMAC
# see SECRET word in the import lines above

# use the | to avoid bugs with App Engine
def make_secure_val(val):
    return '%s|%s' % (val, hmac.new(secret.SECRET, val, hashlib.sha256).hexdigest())

def check_secure_val(secure_val):
    val = secure_val.split('|')[0]
    if secure_val == make_secure_val(val):
        return val

################################################
####### U4      (PASSWORD HASHING)       #######
################################################

##-------BCRYPT VERSION-------##
# this is using modified pure python bcrypt to work on app engine
# http://code.google.com/p/py-bcrypt/source/browse/README

def make_salt():
    #pure python is too slow for more than 5 rounds
    return bcrypt.gensalt(log_rounds = 5)

def make_pw_hash(name, pw, salt = None):
    if not salt:
        salt = make_salt()
    h = bcrypt.hashpw(name + pw, salt)
    return '%s,%s' % (salt, h)

def valid_pw(name, password, h):
    salt = h.split(',')[0]
    return h == make_pw_hash(name, password, salt)

################################################
#############  Signup  Regular exp   ###########
################################################

user_re = re.compile(r"^[a-zA-Z0-9_-]{3,20}$")
def valid_username(username):
    return username and user_re.match(username)

password_re = re.compile(r"^.{3,20}$")
def valid_password(password):
    return password and password_re.match(password)

email_re = re.compile(r"^[\S]+@[\S]+\.[\S]+$")
def valid_email(email):
    return not email or email_re.match(email)

#-----------------------------------------------#
############### UTILS Various ###################
#-----------------------------------------------#
# this is function to alternate coloring of rows in UI
# this works for Jinja2 templating (refer to ao-wiki where it works)
def gray_style(list):
    # enumerate is "return elements of the list in a tuple along
    # ...with the index of that element in a list
    for n, x in enumerate(list):
        if n % 2 == 0:
            # yield is how you build generators
            yield x, ''
        else:
            yield x, 'gray'

#-----------------------------------------------#
############### JINJA2 Config ###################
#-----------------------------------------------#

template_dir = os.path.join(os.path.dirname(__file__), 'template')

jinja_env = jinja2.Environment(loader = jinja2.FileSystemLoader(template_dir),
    autoescape = True)

def render_str(template, **params):
    t = jinja_env.get_template(template)  #2 - loads the template
    return t.render(params)     #3 - renders the template


class BaseHandler(webapp2.RequestHandler):
    def write(self, *a, **kw):
        self.response.out.write(*a, **kw)

    def render_str(self, template, **params):
        params['user'] = self.user
        params['gray_style'] = gray_style
        t = jinja_env.get_template(template)
        return t.render(params)

    def b_render(self, template, **kw):
        self.write(self.render_str(template, **kw))

    # where 'd' is anything that is valid .json
    def render_json(self, d):
        json_txt = json.dumps(d)
        self.response.headers['Content-Type'] = 'application/json; charset=UTF-8'
        self.write(json_txt)

    # can modify to have expiration
    def set_secure_cookie(self, name, val):
        cookie_val = make_secure_val(val)
        self.response.headers.add_header(
            'Set-Cookie',
            '%s=%s; Path=/' % (name, cookie_val))

    def read_secure_cookie(self, name):
        cookie_val = self.request.cookies.get(name)
        return cookie_val and check_secure_val(cookie_val)

    def login(self, user):
        self.set_secure_cookie('user_id', str(user.key.id()))

    # set the cookie value to nothing to delete the cookie, so long as it has same path to overwrite
    def logout(self):
        self.response.headers.add_header('Set-Cookie', 'user_id=; Path=/')

    # in AE its a function that gets called before every request
    # check for the user_id and if it exists store in self.user the actual user object
    # so initialize function gets run on every request and checks to see if user logged in or not
    def initialize(self, *a, **kw):
        webapp2.RequestHandler.initialize(self, *a, **kw)
        uid = self.read_secure_cookie('user_id')
        # the actual user object
        self.user = uid and User.by_id(int(uid))
        if self.request.url.endswith('.json'):
            self.format = 'json'
        else:
            self.format = 'html'

    def notfound(self):
        self.error(404)
        self.write('<h1>404: Not Found</h1> Sorry, my friend, but that page does not exist.')

#-----------------------------------------------#
############### Database Config #################
#-----------------------------------------------#

############### USERS DATASTORE  #################

# this creates the ancestor element in the database to...
# ...store our users in user groups
def users_key(group = 'default'):
    return ndb.Key('User', group)

class User(ndb.Model):
    name = ndb.StringProperty(required = True)
    pw_hash = ndb.StringProperty(required = True)
    email = ndb.StringProperty()

    # these are kind of convenience functions called decorators
    # what this does is say you can call this method on this User object,
    # but it doesnt have to be an instance (normally referred as self),
    # in this case, cls means class and replaces self, so its referring to
    # the class User, not an actual instance of the class User that is an object...
    #---- so for example we can code --> User.by_id ----#
    @classmethod
    def by_id(cls, uid):
        return cls.get_by_id(uid, parent = users_key())

    # need to read up on the datastore procedural code
    # we could use Gql below, but instead use procedural
    @classmethod
    def by_name(cls, name):
        # could code similar Gql 'select * from user where name = name'
        # calling .get() returns the first instance
        # User. should be cls., this is a bug in waiting
        u = cls.query().filter(cls.name == name).get()
        return u

    # creates a new user object but doesnt actually save it in DB
    @classmethod
    def register(cls, name, pw, email = None):
        pw_hash = make_pw_hash(name, pw)
        return User(parent = users_key(),
            name = name,
            pw_hash = pw_hash,
            email = email)

    @classmethod
    def login(cls, name, pw):
        u = cls.by_name(name)
        if u and valid_pw(name, pw, u.pw_hash):
            return u

############### BLOG Datastore PART!!!!!! #################

# this is our Post database
class Post(ndb.Model):
    subject = ndb.StringProperty(required = True)
    content = ndb.TextProperty(required = True)
    created = ndb.DateTimeProperty(auto_now_add = True)
    last_modified = ndb.DateTimeProperty(auto_now = True)

    @staticmethod
    #''' this is for the history capability.  Every post has a name
    #''' at it will always be posts with a parent that is Post: name='path'
    def blog_key(path = 'default'): #(name = 'default')
        return ndb.Key('Post', path)
        #def blog_key(path):
    #    return ndb.Key(Post, path)

    # this is the main handler for content shown on the templates, using markdown
    def p_render(self):
        #self._render_text = self.content.replace('\n', '<br>')
        self._render_text = markdown.markdown(self.content, lazy_ol=False)
        return render_str("post.html", p = self)

    @classmethod
    # for looking up a post by URL, runs datastore query that says...
    #..."get me all the posts for this URL, sorted by creation time
    #... and cls refers to Post, it is a class method and we are in Post
    def by_path(cls, path):
        q = cls.query(ancestor = cls.blog_key(path)).order(-Post.created)
        return q

    @classmethod
    # looking up older versions of a post, can lookup by ID in addition to path
    def by_id(cls, post_id, path):
        return cls.get_by_id(post_id, parent = cls.blog_key(path))

    # for .json output create dictionary representation of the post itself..
    # then pass it into python's .json library for final output
    def as_dict(self):
        # format string "print date nicely in what locale you are in"
        time_fmt = '%c'
        d = {'subject': self.subject,
             'content': self.content,
             'created': self.created.strftime(time_fmt),
             'last_modified': self.last_modified.strftime(time_fmt)}
        return d

############### END BLOG Database PART!!!!!!##############


################################################
####### Various ----------------------   #######
################################################


################################################
####### Handlers: Signup, Login, Logout  #######
################################################

class Signup(BaseHandler):
    def get(self):
        # next two lines disable signup screen unless username is bob or Bob
        if self.user is not User.by_name('bob'):
            self.redirect('/login')
            # next_url is for the history function, using referrer in header to keep track of what
        # URL the form should go to after user submits it
        # the header element referer is always the page user was on for previous page
        next_url = self.request.headers.get('referer', '/')
        # referer gets sent into the form as input type hidden and redirect the user back to page they were on,
        # it is the URL to goto next after user is done signing in
        self.b_render('signup.html', next_url = next_url)

    def post(self):
        # this is a solid design pattern
        have_error = False
        next_url = str(self.request.get('next_url'))
        # dont want to redirect a new user back to the login screen
        if not next_url or next_url.startswith('/login'):
            next_url = '/'

        self.username = self.request.get('username')
        self.password = self.request.get('password')
        self.verify = self.request.get('verify')
        self.email = self.request.get('email')

        params = dict(username = self.username, email = self.email)

        if not valid_username(self.username):
            params['username_error'] = "That's not a valid username."
            have_error = True

        if not valid_password(self.password):
            params['password_error'] = "That wasn't a valid password."
            have_error = True

        elif self.password != self.verify:
            params['verify_error'] = "Your passwords didn't match."
            have_error = True

        if not valid_email(self.email):
            params['email_error'] = "That's not a valid email."
            have_error = True

        if have_error:
            self.b_render('signup.html', **params)
        else:
            # make sure the user doesn't already exist
            u = User.by_name(self.username)
            if u:
                msg = 'That user already exists.'
                self.b_render('signup.html', username_error = msg)
            else:
                u = User.register(self.username, self.password, self.email)
                u.put()

                self.login(u)
                self.redirect(next_url)

class Login(BaseHandler):
    def get(self):
        next_url = self.request.headers.get('referer', '/')
        self.b_render('login.html', next_url = next_url)

    def post(self):
        username = self.request.get('username')
        password = self.request.get('password')

        next_url = str(self.request.get('next_url'))
        if not next_url or next_url.startswith('login'):
            next_url = '/'

        # this is the class method in User
        u = User.login(username, password)
        if u:
            self.login(u)
            self.redirect(next_url)
        else:
            msg = 'Invalid login'
            self.b_render('login.html', error = msg)


class Logout(BaseHandler):
    def get(self):
        # all of the next_url is good target to get pulled into the handler
        next_url = self.request.headers.get('referer', '/')
        self.logout()
        # redirect the user back to the referer
        self.redirect(next_url)

####### END Handlers: Signup, Login, Logout  #######


##########################################################
#!!!!!!!!!!!!!!    BLOG Classes, FIX THIS !!!!!!!!!!!!!!!#
##########################################################


# handler for the main landing page
class BlogFront(BaseHandler):
    def get(self):
        # query for the most recent 3 posts
        posts = Post.query().order(-Post.created).fetch(3)
        onfront = True
        if self.format == 'html':
            self.b_render('front.html', posts = posts, onfront = onfront)
        else:
            return self.render_json([p.as_dict() for p in posts])

# handler for the blog history page
class BlogListing(BaseHandler):
    def get(self):
        # query for all of the posts, order by created time
        posts = Post.query().order(-Post.created)
        if self.format == 'html':
            self.b_render('history.html', posts = posts)
        else:
            return self.render_json([p.as_dict() for p in posts])

# handler for the permalink page
class PermalinkPage(BaseHandler):
    def get(self, post_id):
        # query to get an individual post, per the post_id
        key = ndb.Key(Post, int(post_id), parent=Post.blog_key())
        post = key.get()
        onpermalinkpage = True
        if not post:
            self.error(404)
            return
        if self.format == 'html':
            # base.html could be substituted for permalink.html, with small mod to templates
            # main reason to keep permalink.html is perhaps want more customized page later
            self.b_render("permalink.html", post = post, onpermalinkpage = onpermalinkpage, post_id = post_id)
        else:
            self.render_json(post.as_dict())

# handler for the newpost page
class NewPost(BaseHandler):
    def get(self):
        if not self.user:
            self.redirect('/login')

        self.b_render("newpost.html")

    def post(self):
        if not self.user:
            self.error(400)
            return
        subject = self.request.get('subject')
        content = self.request.get('content')
        if subject and content:
            p = Post(parent = Post.blog_key(), subject = subject, content = content)
            p.put()
            # if want to change to SOE URL's, this is where the key_id is connected to the URL
            self.redirect('/%s' % str(p.key.id()))
        else:
            error = "subject and content, please!"
            self.b_render("newpost.html", subject=subject, content = content, error=error)

# handler to edit posts, retrieve from datastore, enable to change and save again
class EditPost(BaseHandler):
    def get(self, post_id):
        if not self.user:
            self.redirect('login')
            #pass in the post_id, remove '/' from post_id, retrieve the post from datastore
        post_id = post_id[1:]
        key = ndb.Key(Post, int(post_id), parent=Post.blog_key())
        post = key.get()

        if not post:
            self.error(404)
            return

        if self.format == 'html':
            self.b_render("editpost.html", post = post)

    def post(self,post_id):
        if not self.user:
            self.error(400)
            return
        post_id = post_id[1:]
        key = ndb.Key(Post, int(post_id), parent=Post.blog_key())
        # the old post with content and subject
        old_post = key.get()
        content = self.request.get('content')
        subject = self.request.get('subject')

        # what to do when empty content is submitted? just skip it for now
        if not (old_post or content):
            # this could be expanded to just enter in some default content
            #error = "Add some content before submitting, please!"
            #self.b_render("edit.html", content=content, error=error)
            return

        # if not an old version then must be creating a new page
        elif not old_post or old_post.content != content or old_post.subject != subject:
            # this creates a new version for every page and stores all versions in datastore
            old_post.content = content
            old_post.subject = subject
            old_post.put()

        # redirect to the path (not /_edit) so can view the new content
        self.redirect("/"+ post_id)



#--------------------------------------------------------#
PAGE_RE = r'(/(?:[a-zA-Z0-9_-]+/?)*)'
# remember these urls get matched in order
app = webapp2.WSGIApplication([
    ('/signup/?', Signup),
    ('/login/?', Login),
    ('/logout/?', Logout),
    ('/?(?:\.json)?', BlogFront),
    #('/'+PAGE_RE, PostPage),
    ('/([0-9]+)(?:\.json)?', PermalinkPage),
    ('/newpost/?', NewPost),
    ('/_edit/?'+PAGE_RE, EditPost),
    ('/history/?', BlogListing)],
    debug=True)

